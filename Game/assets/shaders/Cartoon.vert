uniform mat4 g_WorldViewProjectionMatrix;
uniform vec3 g_CameraPosition;
uniform mat3 g_WorldMatrixInverseTranspose;

attribute vec3 inPosition;
attribute vec3 inNormal;

varying vec3 normal;
varying vec3 viewVector;

void main(){
	gl_Position = g_WorldViewProjectionMatrix * vec4(inPosition, 1.0);
	normal = inNormal * g_WorldMatrixInverseTranspose;
	viewVector = g_CameraPosition - inPosition;


}
