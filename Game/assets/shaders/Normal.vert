uniform mat4 g_WorldViewProjectionMatrix;
uniform mat3 g_WorldMatrixInverseTranspose;
attribute vec3 inPosition;
attribute vec3 inNormal;

varying vec3 normal;

void main(){
	normal = inNormal * g_WorldMatrixInverseTranspose;
	gl_Position = g_WorldViewProjectionMatrix * vec4(inPosition, 1.0);
}
