uniform vec4 m_Color;
uniform vec4 m_AmbientLight;
uniform vec3 m_AmbientLightDirection;
uniform vec2 g_ResolutionInverse;
uniform sampler2D m_DepthBuffer;
uniform sampler2D m_NormalBuffer;

vec2 m_NearFar = vec2( 0.1, 1000.0 );

varying vec3 normal;
varying vec3 viewVector;

void main(){


	// Diffuse
	float lightNormalCos = max(0.0, dot(normalize(normal), normalize(m_AmbientLightDirection)));
	vec3 diffuse_color = m_Color.rgb * (lightNormalCos - mod(lightNormalCos, 0.25));

	// Specular
	vec3 reflectionVector = normalize(reflect(-m_AmbientLightDirection, normal));
	float specularNormalCos = pow(max(0.0, dot(reflectionVector, normalize(viewVector))), 30.0);

	vec4 specular_color = vec4(1.0,1.0,1.0,1.0) * smoothstep(0.3, 0.7, specularNormalCos);



	

	vec2 texcoords = vec2(gl_FragCoord.x, gl_FragCoord.y) * g_ResolutionInverse;
	vec2 coords;
	vec4 depth;
	float pix[9];

	int k = -1;
	for(int i=-1; i < 2; i++) {
		for(int j=-1; j < 2; j++) {
			k++;
			coords = texcoords + vec2(i,j) * g_ResolutionInverse;
			depth =  texture2D(m_DepthBuffer,coords);

			float a = m_NearFar.y / (m_NearFar.y - m_NearFar.x);
   			float b = m_NearFar.y * m_NearFar.x / (m_NearFar.x - m_NearFar.y);
   			float z = b / (depth.z - a); //Ska det vara z h�r?

			pix[k] = z;
		}
	}

	float deltaDepth = (abs(pix[1]-pix[7]) + abs(pix[5]-pix[3]) + abs(pix[0]-pix[8]) + abs(pix[2]-pix[6]))/4.;

	
	vec4 n;
	vec3 norm[9];
	k = -1;
	float angle = 0.0;

	vec3 no = texture2D(m_NormalBuffer,texcoords).xyz;

	for(int i=-1; i < 2; i++) {
		for(int j=-1; j < 2; j++) {
			k++;
			coords = texcoords + vec2(i,j) * g_ResolutionInverse;
			n =  texture2D(m_NormalBuffer,coords);
			angle = max(angle, acos(dot(normalize(no), normalize(n.xyz))));
		}
	}




	if((deltaDepth > 0.1 && pix[5] < 0.9) || angle > 0.1) {
		gl_FragColor = vec4(0,0,0,1);
	} else {
		gl_FragColor = vec4(diffuse_color.rgb, 1.0) + specular_color +  m_AmbientLight;
	}
	
	//gl_FragColor = vec4(normalize(normal)/2.0 + 0.5,1.0);
	//gl_FragColor = vec4(angle, angle,angle, 1);
	
}


float smoothstep(float min, float max, float x)
{
	float t = clamp((x - min)/(max - min), 0.0, 1.0);
	return t*t*(3.0 - (2.0*t));
	
}


