uniform vec4 m_Color;
varying vec3 normal;

void main(){
	gl_FragColor = vec4(m_Color.rgb, 1.0);
}
