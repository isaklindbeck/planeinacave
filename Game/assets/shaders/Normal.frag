varying vec3 normal;

void main(){
	gl_FragColor = vec4(normal/2.0 + 0.5,1.0);
}
