package tools;

import java.util.Random;

import jme3tools.optimize.GeometryBatchFactory;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;

public class BatchGenerator {
	
	public static Spatial MultiCube(float xSide, float ySide, float zSide, float cubeSide, float frequency) {
		Node batchNode = new Node();
		Random rand = new Random();
		
		Box box = new Box(cubeSide,cubeSide,cubeSide);
		
		float a = -Math.abs(xSide);
		float b = -Math.abs(ySide);
		float c = -Math.abs(zSide);
		float pi = (float) (2f*Math.PI);

		while(a < Math.abs(xSide)) {
			while(b < Math.abs(ySide)) {
				while(c < Math.abs(zSide)) {
					float r1 = pi * rand.nextFloat();
					float r2 = pi * rand.nextFloat();
					float r3 = pi * rand.nextFloat();
					
					Geometry rock = new Geometry("rock", box);
					rock.rotate(r1, r2, r3);
					rock.setLocalTranslation(a, b, c);
					batchNode.attachChild(rock);
					float cInc = rand.nextFloat() * frequency;
					c += cInc;
				}
				float bInc = rand.nextFloat() * frequency;
				b += bInc;
				c = -Math.abs(zSide);
			}
			float aInc = rand.nextFloat() * frequency;
			a += aInc;
			b = -Math.abs(ySide);
		}
		Spatial s = GeometryBatchFactory.optimize(batchNode);
		return s;
	}
}
