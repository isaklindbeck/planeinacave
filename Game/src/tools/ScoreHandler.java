package tools;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class ScoreHandler {

	public static void write(ArrayList<String> score) {
		
		StringBuilder sb = new StringBuilder();
		sb.append(score.get(0));
		for (int i = 1; i < score.size(); i++) {
			sb.append('\n' + score.get(i));
		}
		
		PrintWriter writer = null;
		try {
			writer = new PrintWriter("highScore", "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		writer.println(sb.toString());
		writer.close();
	}
	
	public static ArrayList<String> read() {
	    BufferedReader br;
	    ArrayList<String> ret = new ArrayList<String>();
		try {
			br = new BufferedReader(new FileReader("highScore"));

			String line = br.readLine();
			
			while (line != null) {
				ret.add(line);
				line = br.readLine();
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}
}
