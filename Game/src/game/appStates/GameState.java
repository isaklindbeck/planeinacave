package game.appStates;

import java.util.ArrayList;

import tools.ScoreHandler;

import model.Player;
import model.World;
import game.MainGame;
import game.MainGame.AppState;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.input.InputManager;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import com.jme3.texture.Texture2D;

import control.Control;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.builder.TextBuilder;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class GameState extends AbstractAppState implements ScreenController {

	private MainGame app;
	private World world;
	private Vector3f lightDirection;
	private ColorRGBA ambientLight;
	private Player player;
	private Camera cam;
	
	private Node gameRootNode;
	private Node gameToonNode;
	
	private Nifty nifty;
	private NiftyJmeDisplay nd;
	
	private AudioNode audio_music;
	private AudioNode[] audio = new AudioNode[4];
	
	private boolean gameOver;
	private int points;
	private boolean hasScore;
	private int scoreIndex;
	private Control crtl;
	
	public GameState(AppStateManager stateManager, Application app) {
		super();
	}
	
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);
		
		nd = new NiftyJmeDisplay(app.getAssetManager(),
				app.getInputManager(), app.getAudioRenderer(),
				app.getGuiViewPort());
		nifty = nd.getNifty();
		nifty.fromXml("interface/gameInterface.xml", "start", this);
		app.getGuiViewPort().addProcessor(nd);
		
		
		app.getInputManager().setCursorVisible(false);
		this.app = (MainGame) app;
		cam = this.app.getCamera();
			
		gameRootNode = new Node();
		gameToonNode = new Node();
		
		this.app.getRootNode().attachChild(gameRootNode);
		this.app.getToonNode().attachChild(gameToonNode);
		
		lightDirection = new Vector3f(1f, 3f, -1f);
		ambientLight = new ColorRGBA(0,0,0.4f, 1.0f);
		player = new Player(new Vector3f(0,-1,0), this);
		world = new World(this, player);
		crtl = new Control(this, player);	

		initAudio();
	}
	
	public Node getRootNode() {
		return gameRootNode;
	}
	
	public Node getToonNode() {
		return gameToonNode;
	}
	
	public Vector3f getLightDirection() {
		return lightDirection;
	}
	
	public ColorRGBA getAmbientLight() {
		return ambientLight;
	}
	
	public Texture2D getDepthBuffer() {
		return app.getZBuffer();
	}
	
	public Texture2D getNormalBuffer() {
		return app.getNBuffer();
	}
	
	public AssetManager getAssetManager() {
		return app.getAssetManager();
	}
	
	public InputManager getInputManager() {
		return app.getInputManager();
	}
	
	public void setDamageOpacity(float f) {
		Screen screen = nifty.getScreen("start");
		screen.findElementByName("top").setHeight((int) (150 * f));
	}
	
	public static final int AUDIO_MUSIC = 0;
	public static final int AUDIO_HURT = 1;
	public static final int AUDIO_GAMEOVER = 2;
	public static final int AUDIO_COIN = 3;
	
	public AudioNode getAudioNode(int id) {
		return audio[id];
	}
	
	private void initAudio() {
	    audio_music = new AudioNode(getAssetManager(), "sounds/Airmusic.wav", false);
	    audio_music.setPositional(false);
	    audio_music.setVolume(4);
	    audio_music.setLooping(true);
	    getRootNode().attachChild(audio_music);
	    audio_music.play();
	    audio_music.setPitch(0.5f);
	    
	    AudioNode audio_crash = new AudioNode(getAssetManager(), "sounds/Hurt.wav", false);
	    audio_crash.setPositional(false);
	    audio_crash.setLooping(false);
	    audio_crash.setVolume(2);
	    getRootNode().attachChild(audio_crash);
	    
	    AudioNode audio_gameover = new AudioNode(getAssetManager(), "sounds/Endofgame2.wav", false);
	    audio_gameover.setPositional(false);
	    audio_gameover.setLooping(false);
	    audio_gameover.setVolume(2);
	    getRootNode().attachChild(audio_gameover);
	    
	    AudioNode audio_coin = new AudioNode(getAssetManager(), "sounds/Pickup_Coin.wav", false);
	    audio_coin.setPositional(false);
	    audio_coin.setLooping(false);
	    audio_coin.setVolume(2);
	    getRootNode().attachChild(audio_coin);
	    
	    audio[0] = audio_music;
	    audio[1] = audio_crash;
	    audio[2] = audio_gameover;
	    audio[3] = audio_coin;
	    
	}
	
	public void incPoint() {
		points += 10;
	}
	
	public void setName() {
		Screen screen = nifty.getScreen("end");
		String inName = screen.findElementByName("input").findNiftyControl("text_input", TextField.class).getRealText();
		
		if(hasScore) {
			
			ArrayList<String> score = ScoreHandler.read();
			
			for (int i = 0; i < score.size(); i++) {
				System.out.println(score.get(i));
			}
			
			if(scoreIndex != -1) {				
				score.add(scoreIndex, "" + points);
				score.add(scoreIndex, inName);
			} else {
				score.add(inName);
				score.add("" + points);
			}

			while(score.size() > 12) {
				score.remove(score.size() -1);
			}
			
			ScoreHandler.write(score);
			
			screen.findElementByName("input").setVisible(false);
		}
		
	}
	
	public void gameOver() {
		Screen screen = nifty.getScreen("end");
		
		int dist = (int) player.getPosition().z;
		ArrayList<String> score = ScoreHandler.read();
		
		hasScore = false;
		scoreIndex = -1;
		
		for (int i = 1; i < score.size(); i+=2) {
			int p = Integer.parseInt(score.get(i));
			if(p < points) {
				scoreIndex = i - 1;
				score.add(scoreIndex, "" + points);
				score.add(scoreIndex, "YOUR PLACE");
				screen.findElementByName("input").setVisible(true);
				hasScore = true;
				break;
			}
		}
		
		if(scoreIndex == -1 && score.size() < 12) {
			score.add("YOUR PLACE");
			score.add("" + points);
			screen.findElementByName("input").setVisible(true);
			hasScore = true;
		} else if(scoreIndex == -1){
			screen.findElementByName("input").setVisible(false);
		}
		
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < Math.min(score.size(), 12); i+=2) {
			sb.append(score.get(i));
			sb.append(": " + score.get(i + 1) + '\n');
		}
		


		TextBuilder tb = new TextBuilder();
		tb.font("font/impact144.fnt");		
		tb.align(Align.Center);
		
		tb.text("Points:" + '\n' + points + '\n' + "Distance:" + '\n' + dist);
		tb.build(nifty, screen, screen.findElementByName("result"));	
		
		tb.text("HighScore");
		tb.build(nifty, screen, screen.findElementByName("highscore"));
		
		tb.font("font/impact72.fnt");		
		tb.text(sb.toString());
		tb.build(nifty, screen, screen.findElementByName("highscore"));
		
//		ControlBuilder cb = new ControlBuilder("text_input", "textfeild");
//		cb.
//		cb.build(nifty, screen, screen.findElementByName("highscore"));
		
		nifty.gotoScreen("end");
		gameOver = true;
		audio[2].play();
		
		app.getInputManager().setCursorVisible(true);
	}
	
	public boolean isGameOver() {
		return gameOver;
	}
	
	
	@Override
	public void cleanup() {
		super.cleanup();
		
		getInputManager().removeListener(crtl.getListener());
		
		nd.getNifty().exit();
		nd.cleanup();
		
		audio_music.stop();
		app.getRootNode().detachChild(gameRootNode);
		app.getToonNode().detachChild(gameToonNode);
		app.getStateManager();
		points = 0;
		gameOver = false;
	}
	
	@Override
	public void update(float tpf) {
//		cam.setLocation(new Vector3f(0,40,5));
//		cam.lookAt(new Vector3f(0, 0, 5), new Vector3f(1, 0, 0));

		world.update(tpf);
		setDamageOpacity(Math.min(1 - player.getHealth(), 1.0f));
		
		if(!gameOver) {
			cam.setLocation(player.getPosition().add(0, 5.0f, -15.0f));
			cam.lookAt(player.getPosition(), new Vector3f(0, 1, 0));
			
			if(player.getHealth() < 0) {
				gameOver();
			}
		}
	}

	@Override
	public void bind(Nifty arg0, Screen arg1) { }
	@Override
	public void onEndScreen() { }
	@Override
	public void onStartScreen() {	}
	
	public void quit() {
		app.changeState(this, AppState.MENU);
	}
}
