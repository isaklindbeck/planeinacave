package game.appStates;

import java.util.ArrayList;

import tools.ScoreHandler;
import game.MainGame;
import game.MainGame.AppState;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.niftygui.NiftyJmeDisplay;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.TextBuilder;
import de.lessvoid.nifty.builder.ElementBuilder.Align;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class MainMenu extends AbstractAppState implements ScreenController {

	private MainGame app;
	private NiftyJmeDisplay nd;
	private Nifty nifty;

	public MainMenu(MainGame app) {
		this.app = app;		
	}
	
	public void initialize(AppStateManager stateManager, Application app) {
		app.getInputManager().setCursorVisible(true);
		nd = new NiftyJmeDisplay(app.getAssetManager(),
				app.getInputManager(), app.getAudioRenderer(),
				app.getGuiViewPort());
		nifty = nd.getNifty();
		nifty.fromXml("interface/menuInterface.xml", "start", this);
		app.getGuiViewPort().addProcessor(nd);
		
		Screen screen = nifty.getScreen("start");
		
		TextBuilder tb = new TextBuilder();
		tb.font("font/impact144.fnt");		
		tb.text("New Game");
		tb.align(Align.Center);
		tb.build(nifty, screen, screen.findElementByName("newGame"));
		
		tb.text("HighScore");
		tb.build(nifty, screen, screen.findElementByName("highscore"));
		
		tb.text("About");
		tb.build(nifty, screen, screen.findElementByName("about"));

		tb.text("Exit");
		tb.build(nifty, screen, screen.findElementByName("exit"));
		
		screen = nifty.getScreen("highscore");
		
		tb = new TextBuilder();
		tb.font("font/impact72.fnt");		
		tb.align(Align.Center);
		
		String text = "HighScore";
		tb.text(text);
		tb.build(nifty, screen, screen.findElementByName("text"));
		
		ArrayList<String> score = ScoreHandler.read();
		
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < Math.min(score.size(), 12); i+=2) {
			sb.append(score.get(i));
			sb.append(": " + score.get(i + 1) + '\n');
		}
		
		text = sb.toString();
		tb.font("font/impact32.fnt");
		tb.text(text);
		tb.build(nifty, screen, screen.findElementByName("text"));
		
		
		
		screen = nifty.getScreen("about");
		
		tb = new TextBuilder();
		tb.font("font/impact72.fnt");		
		tb.align(Align.Center);
		
		text = "Authors";
		tb.text(text);
		tb.build(nifty, screen, screen.findElementByName("text"));
		
		text = "Isak Lindbeck, Malin H�gg";
		tb.font("font/impact32.fnt");	
		tb.text(text);
		tb.build(nifty, screen, screen.findElementByName("text"));
		
		text = "Description";
		tb.font("font/impact72.fnt");	
		tb.text(text);
		tb.build(nifty, screen, screen.findElementByName("text"));
		
		text = "PlaneInACave was developed in Java using JMonkeyEngine" + '\n' +"as a part of the course EDAN35, High Performance Graphics at LTH.";
		tb.font("font/impact32.fnt");	
		tb.text(text);
		tb.build(nifty, screen, screen.findElementByName("text"));
		
		text = "Music";
		tb.font("font/impact72.fnt");	
		tb.text(text);
		tb.build(nifty, screen, screen.findElementByName("text"));
		
		text = "www.nosoapradio.us";
		tb.font("font/impact32.fnt");	
		tb.text(text);
		tb.build(nifty, screen, screen.findElementByName("text"));
		
		
		
		
		app.getInputManager().addMapping("esc", new KeyTrigger(KeyInput.KEY_ESCAPE));
		app.getInputManager().addListener(actionListener, "esc");
	}
	
	protected ActionListener actionListener = new ActionListener() {

		@Override
		public void onAction(String name, boolean keyPressed, float tpf) {
			if (name.equals("esc")) {
				nifty.gotoScreen("start");
			}
		}
		
	};

	@Override
	public void bind(Nifty arg0, Screen arg1) { }
	@Override
	public void onEndScreen() { }
	@Override
	public void onStartScreen() { }
	
	public void newGame() {
		app.changeState(this, AppState.GAME);
		nd.getNifty().exit();
		nd.cleanup();
	}
	
	public void highscore() {		
		nifty.gotoScreen("highscore");
		
	}
	
	public void about() {		
		nifty.gotoScreen("about");
	}
	
	public void exit() {
		app.stop();
	}
	
	@Override
	public void cleanup() {
		super.cleanup();
		app.getInputManager().removeListener(actionListener);
	}
}
