package game.appStates;

import game.MainGame;
import game.MainGame.AppState;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.niftygui.NiftyJmeDisplay;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;

public class SplashScreen extends AbstractAppState {
	private MainGame app;
	
	private Nifty nifty;
	private NiftyJmeDisplay nd;
	private float time;

	public SplashScreen(MainGame app) {
		this.app = app;
	}
	
	public void initialize(AppStateManager stateManager, Application app) {
		nd = new NiftyJmeDisplay(app.getAssetManager(),
				app.getInputManager(), app.getAudioRenderer(),
				app.getGuiViewPort());
		nifty = nd.getNifty();
		nifty.fromXml("interface/splashInterface.xml", "start");
		app.getGuiViewPort().addProcessor(nd);
	
		time = 4;
	}
	
	public void update(float tpf) {
		time -= tpf;
		Screen screen = nifty.getScreen("start");
		screen.findElementByName("img1");
		if(time <= 0) {
			nd.getNifty().exit();
			nd.cleanup();
			app.changeState(this, AppState.MENU);
			app.getGuiViewPort().removeProcessor(nd);
		}
		
	}
}
