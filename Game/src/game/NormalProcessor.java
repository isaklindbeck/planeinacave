package game;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.post.SceneProcessor;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.texture.FrameBuffer;

public class NormalProcessor implements SceneProcessor {

	RenderManager renderManager;

	Material normalMaterial;

	public NormalProcessor(AssetManager manager) {

		normalMaterial = new Material(manager, "shaders/Normal.j3md");

	}

	public void initialize(RenderManager rm, ViewPort vp) {

		renderManager = rm;

	}

	public void reshape(ViewPort vp, int w, int h) {

	}

	public boolean isInitialized() {

		return renderManager != null;

	}

	public void preFrame(float tpf) {

	}

	public void postQueue(RenderQueue rq) {

		renderManager.setForcedMaterial(normalMaterial);

	}

	public void postFrame(FrameBuffer out) {

		renderManager.setForcedMaterial(null);

	}

	public void cleanup() {

		renderManager.setForcedMaterial(null);

	}

}
