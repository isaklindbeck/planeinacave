package game;

import game.appStates.GameState;
import game.appStates.MainMenu;
import game.appStates.SplashScreen;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.math.ColorRGBA;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;
import com.jme3.texture.FrameBuffer;
import com.jme3.texture.Texture2D;
import com.jme3.texture.Image.Format;

public class MainGame extends SimpleApplication {

	private static MainGame app;

	private GameState gameState;
	private SplashScreen splash;
	private MainMenu mainMenu;

	private Texture2D depthBufferTexture;
	private Texture2D normalBufferTexture;
	
	private Node toonNode;
	
	public static void main(String[] args) {
		Logger.getLogger("").setLevel(Level.SEVERE);
		app = new MainGame();
//		app.setShowSettings(false);
//		app.setPauseOnLostFocus(false);
		AppSettings settings = new AppSettings(true);
//		settings.setResolution(1024,600);
//		settings.setBitsPerPixel(32);
		settings.setFrameRate(120);
//      settings.setSamples(16);
		app.setSettings(settings);
		app.start();
	}
	
	@Override
	public void simpleInitApp() {
		this.inputManager.deleteMapping( SimpleApplication.INPUT_MAPPING_EXIT );
//		flyCam.setMoveSpeed(60f);
		flyCam.setEnabled(false);
		
		int w = getCamera().getWidth();
		int h = getCamera().getHeight();
		
		toonNode = new Node();
		rootNode.attachChild(toonNode);
		
		FrameBuffer zBuffer = new FrameBuffer(w, h, 0);
		zBuffer.setDepthBuffer(Format.Depth32);
		depthBufferTexture = new Texture2D(w, h, Format.Depth32);
		zBuffer.setDepthTexture(depthBufferTexture);
		
		ViewPort depthView = getRenderManager().createPreView("depthView", cam);
		depthView.setOutputFrameBuffer(zBuffer);
		depthView.attachScene(toonNode);
		depthView.setClearFlags(true, true, true);
		normalBufferTexture = new Texture2D(w, h, Format.RGBA8);
		zBuffer.setColorBuffer(Format.RGBA8);
		zBuffer.setColorTexture(normalBufferTexture);
		depthView.addProcessor(new NormalProcessor(assetManager));
		
		viewPort.setBackgroundColor(ColorRGBA.Black);
		
		gameState = new GameState(stateManager, app);
		mainMenu = new MainMenu(app);
		splash = new SplashScreen(app);
		
		stateManager.attach(splash);
		splash.setEnabled(true);
	}
	
	public enum AppState {
		GAME, MENU, SPLASH;
	}
	
	public void changeState(AbstractAppState from, AppState to) {
		from.setEnabled(false);
		stateManager.detach(from);
		
		switch (to) {
		case SPLASH:
			stateManager.attach(splash);
			splash.setEnabled(true);
			break;
		case MENU:
			stateManager.attach(mainMenu);
			mainMenu.setEnabled(true);
			break;
		case GAME:
			stateManager.attach(gameState);
			gameState.setEnabled(true);
			break;

		default:
			break;
		}
	}
	
	public Texture2D getZBuffer() {
		return depthBufferTexture;
	}
	
	public Texture2D getNBuffer() {
		return normalBufferTexture;
	}
	
	public Node getToonNode() {
		return toonNode;
	}

}