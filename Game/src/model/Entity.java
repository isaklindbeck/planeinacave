package model;

import game.appStates.GameState;

import com.jme3.asset.AssetManager;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public abstract class Entity {
	protected Node mainNode;
	protected Node rootNode;
	protected AssetManager assetManager;
	protected GameState gameState;
	protected Spatial model;
	protected float width;
	protected float height;
	
	public Entity(Vector3f pos, GameState gameState, float width, float height) {
		this.gameState = gameState;
		this.width = width;
		this.height = height;
		this.mainNode = new Node();
		this.rootNode = gameState.getRootNode();
		this.assetManager = gameState.getAssetManager();

		this.mainNode.setLocalTranslation(pos);		
		initialize();
		
		gameState.getRootNode().updateGeometricState();
	
	}
	
	protected abstract void initialize();
	
	public Vector3f getPosition() {
		return mainNode.getWorldTranslation();
	}
	
	public void setPosition(Vector3f pos) {
		mainNode.setLocalTranslation(pos);
	}
	
	public void detach() {
		mainNode.removeFromParent();
	}
	
	public void attach() {
		rootNode.attachChild(mainNode);
	}
	
	public Node getMainNode() {
		return mainNode;
	}
	
	public boolean contains(Node point) {
		float x = point.getWorldTranslation().x;
		float y = point.getWorldTranslation().y;
		float z = point.getWorldTranslation().z;
		
		float px = getPosition().x;
		float py = getPosition().y;
		float pz = getPosition().z;
		
		if(x > px - width/2.0f && x < px + width/2.0f) {
			if(y > py - height/2.0f && y < py + height/2.0f) {
				if(z > pz - 5 && z < pz + 5) {
					return true;
				}	
			}
		}
		
		return false;
	}
	
	public void update(float tpf) {
		
	}
}
