package model;

import tools.BatchGenerator;

import game.appStates.GameState;

import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

public class GroundBlock extends Entity {
	
	public GroundBlock(Vector3f pos, GameState gameState) {
		super(pos, gameState,10,10);
		this.rootNode = gameState.getToonNode();
	}

	@Override
	protected void initialize() {
		Material mat = new Material(assetManager, "shaders/Cartoon.j3md");
		mat.setColor("Color", ColorRGBA.Blue);
		mat.setColor("AmbientLight", gameState.getAmbientLight());
		mat.setVector3("AmbientLightDirection", gameState.getLightDirection());
		mat.setTexture("DepthBuffer", gameState.getDepthBuffer());
		mat.setTexture("NormalBuffer", gameState.getNormalBuffer());
		
		ColorRGBA col = ColorRGBA.randomColor();// ColorRGBA.Gray; //
		mat.setColor("Color", col);
		mat.setColor("AmbientLight", col.mult(0.8f));
		
		model = BatchGenerator.MultiCube(3, 3, 3, 2, 5);
		model.setMaterial(mat);
		mainNode.attachChild(model);
		
//		Box b = new Box(5f,5f,5f);
//		Geometry geo = new Geometry("cube", b);		
//		mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
//		mat.setColor("Color", ColorRGBA.Green);
//		geo.setMaterial(mat);
//		mat.getAdditionalRenderState().setWireframe(true);
//		mainNode.attachChild(geo);
		
	}
	
	public boolean contains(Node point) {
		float x = point.getWorldTranslation().x;
		float y = point.getWorldTranslation().y;
		float z = point.getWorldTranslation().z;
		
		float px = getPosition().x;
		float py = getPosition().y;
		float pz = getPosition().z;
		
		if(x > px - width/2.0f && x < px + width/2.0f) {
			if(y > py - height/2.0f && y < py + height/2.0f) {
				if(z > pz - 5 && z < pz + 5) {
					return model.getWorldBound().contains(point.getWorldTranslation());
				}	
			}
		}
		
		return false;
	}

}
