package model;

import java.util.LinkedList;
import java.util.Random;

import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

import game.appStates.GameState;

public class World {
	
	private LinkedList<Entity> placedEntityList;
	private Player player;
	
	private Coin coin;
	
	private Random rand;
	
	private LinkedList<SideWall> verticalSidePool;
	private LinkedList<SideWall> horizontalSidePool;
	private LinkedList<GroundBlock> blockPool;
	
	private Entity[] lastRow;
	private float nextRowZ;
	private int caveHeight = 12;
	private int caveWidth = 12;
	
	private float invurnavleTime;
	
	private GameState gameState;
	

	public World(GameState gameState, Player player) {
		
		coin = new Coin(Vector3f.ZERO, gameState);
		
		invurnavleTime = 0.5f;
		
		rand = new Random();
		this.gameState = gameState;
		this.player = player;
		player.setPosition(new Vector3f(0,6,0));
		player.attach();
		
		verticalSidePool = new LinkedList<SideWall>(); 
		horizontalSidePool = new LinkedList<SideWall>(); 
		blockPool = new LinkedList<GroundBlock>();
		
		for (int i = 0; i < 50; i++) {
			verticalSidePool.add(new SideWall(Vector3f.ZERO, gameState, caveWidth*10,10));
			horizontalSidePool.add(new SideWall(Vector3f.ZERO, gameState, 10,caveHeight*10));
			blockPool.add(new GroundBlock(Vector3f.ZERO, gameState));
		}
		
		placedEntityList = new LinkedList<Entity>();
		lastRow = new Entity[caveWidth - 2];
		
		nextRowZ = 0;
		
		for (int i = 0; i < 20; i++) {
			generateCaveSides();
		}
		
		for (int i = 0; i < lastRow.length; i++) {
			lastRow[i] = blockPool.poll();
			placedEntityList.add(lastRow[i]);
			float x = (caveWidth-2)/2.0f*10 - i * 10 - 5;
			float y = (rand.nextFloat() * 2 - 1) * (caveHeight-2)/2.0f * 10;
			lastRow[i].setPosition(new Vector3f(x, y, nextRowZ));
			lastRow[i].attach();
		}
		
		for (int i = 0; i < 20; i++) {
			generateRow();
		}
		
	}
	
	private void generateCaveSides() {
		
		Entity topSide = poolGetSideWall(verticalSidePool, caveWidth*10,10);
		Entity bottomSide = poolGetSideWall(verticalSidePool,caveWidth*10,10);
		Entity leftSide = poolGetSideWall(horizontalSidePool,10,caveHeight*10);
		Entity rightSide = poolGetSideWall(horizontalSidePool,10,caveHeight*10);
		
		topSide.setPosition(new Vector3f(0,caveHeight/2.0f * 10,nextRowZ));
		
		bottomSide.setPosition(new Vector3f(0,-caveHeight/2.0f * 10,nextRowZ));
		
		leftSide.setPosition(new Vector3f(-caveWidth/2.0f * 10,0,nextRowZ));
		
		rightSide.setPosition(new Vector3f(caveWidth/2.0f * 10,0,nextRowZ));
		
		topSide.attach();
		bottomSide.attach();
		leftSide.attach();
		rightSide.attach();
		
		placedEntityList.add(topSide);
		placedEntityList.add(bottomSide);
		placedEntityList.add(leftSide);
		placedEntityList.add(rightSide);
		
		nextRowZ += 10;
	}
	
	private void generateRow() {
		
		int coinPos = -1;
		
		if(!coin.isPlaced()) {		
			coinPos = rand.nextInt(lastRow.length);
		}
		
		for (int i = 1; i < lastRow.length - 1; i++) {
			Vector3f lastPos = lastRow[i].getPosition();
			lastRow[i] = poolGetGroundBlock(blockPool);
			placedEntityList.add(lastRow[i]);
			float x = lastPos.x + rand.nextFloat() * 40 - 20;
			x = Math.max(x, -(caveWidth-2)/2.0f * 10);
			x = Math.min(x, (caveWidth-2)/2.0f * 10);
			float y = lastPos.y + rand.nextFloat() * 40 - 20;
			y = Math.max(y, -(caveHeight-2)/2.0f * 10);
			y = Math.min(y, (caveHeight-2)/2.0f * 10);
			float z = nextRowZ;
			lastRow[i].setPosition(new Vector3f(x, y, z));
			//lastRow[i].setPosition(new Vector3f(-(caveWidth/2.0f + i) * 10 + rand.nextInt(7) - 13, Math.max(Math.min(100, lastHeight + (rand.nextInt(7) - 3) * 10), 0), nextRowZ));
			lastRow[i].attach();
			
			if(i == coinPos) {
				lastRow[i].detach();
				blockPool.add((GroundBlock) lastRow[i]);
				coin.setPosition(lastRow[i].getPosition());
				coin.attach();
				placedEntityList.add(coin);
			}
			
		}
		

		
		generateCaveSides();
	
	}
	
	private Entity poolGetSideWall(LinkedList<SideWall> list, float width, float height) {
		if(list.size() < 1) {
			for (int i = 0; i < 10; i++) {
				list.add(new SideWall(Vector3f.ZERO, gameState, width, height));
			}
		}
		return list.poll();
	}
	
	private Entity poolGetGroundBlock(LinkedList<GroundBlock> list) {
		if(list.size() < 1) {
			for (int i = 0; i < 10; i++) {
				list.add(new GroundBlock(Vector3f.ZERO, gameState));
			}
		}
		return list.poll();
	}

	public void update(float tpf) {
		player.update(tpf);
		
		if(gameState.isGameOver()) {
			return;
		}
		
		
		for (int i = 0; i < placedEntityList.size(); i++) {
			Entity e = placedEntityList.get(i);
			if(e.getPosition().getZ() < player.getPosition().getZ() - 20) {
				placedEntityList.remove(i);
				
				if(e instanceof SideWall) {
					SideWall s = (SideWall) e;
					if(s.isVertical()) {
						verticalSidePool.add(s);
					} else {
						horizontalSidePool.add(s);
					}
				} else if(e instanceof GroundBlock) {
					blockPool.add((GroundBlock) e);
				} else if(e instanceof Coin) {
					coin.detach();
				}
				
			} else {
				
				LinkedList<Node> nodes = player.getCollisionNodes();
				
				if(e.contains(nodes.get(2)) ) {
					if(invurnavleTime < 0) {
						player.reduceHealth(0.5f);
						invurnavleTime = 1.0f;
						//System.out.println("H: " + player.getHealth());
					}
				} else if( e.contains(nodes.get(0)) || e.contains(nodes.get(1))){
					if(invurnavleTime < 0) {
						player.reduceHealth(0.2f);
						//player.setPosition(new Vector3f(0f,0f,0f));
						invurnavleTime = 1.0f;
						//System.out.println("H: " + player.getHealth());
					}
				}
				
				
			}
			
			
		}
		
		if(coin.isPlaced()) {
			coin.update(tpf);
			if(player.contains(coin.getMainNode())) {
				gameState.incPoint();
				gameState.getAudioNode(GameState.AUDIO_COIN).play();
				coin.detach();
				player.incSpeed();
			}
		}
		
		if(player.getPosition().getZ() > nextRowZ - 500) {
			generateRow();
		}
		
		float x = player.getPosition().x;
		float y = player.getPosition().y;
		
		if(x < -(caveWidth/2.0f) * 10 || x > (caveWidth/2.0f) * 10 || y < -(caveHeight/2.0f) * 10 || y > (caveHeight/2.0f) * 10 ) {
			player.reduceHealth(2);
		}

		invurnavleTime -= tpf;
	}

}
