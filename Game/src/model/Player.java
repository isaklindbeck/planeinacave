package model;

import java.util.LinkedList;

import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.scene.plugins.blender.BlenderModelLoader;
import com.jme3.scene.shape.Box;

import game.appStates.GameState;

public class Player extends Entity {

	private float maxSpeed = 0.25f;
	private float maxSideSpeed = 0.3f + maxSpeed/4f;
	private float maxVerticalSpeed = 0.15f + maxSpeed/4f;

	private boolean acceleratingUp;
	private boolean acceleratingDown;
	private boolean acceleratingRight;
	private boolean acceleratingLeft;

	private Vector3f forceV;

	private LinkedList<Node> collisionNodes;
	
	private Node leftCollisionNode;
	private Node rightCollisionNode;
	private Node centerCollisionNode;
	private Node collisionMainNode;
	private float health;
	private Geometry boundBox;
	
	
	private static Spatial plane;
	private static Spatial propeller;

	public Player(Vector3f pos, GameState gameState) {
		super(pos, gameState,10,2);
		rootNode = gameState.getToonNode();

	}

	@Override
	protected void initialize() {

		forceV = new Vector3f(0, 0, 0);
		health = 1.0f;
		
		assetManager.registerLoader(BlenderModelLoader.class, "blend");
		plane = assetManager.loadModel("models/plane.blend");
		float pi = (float) Math.PI;
		plane.rotate(0, 0, 0);
		plane.scale(0.5f);

		Material mat = new Material(assetManager, "shaders/Cartoon.j3md");
		mat.setColor("Color", ColorRGBA.Green);
		mat.setColor("AmbientLight", ColorRGBA.Green.mult(0.2f));
		mat.setVector3("AmbientLightDirection", gameState.getLightDirection());
		mat.setTexture("DepthBuffer", gameState.getDepthBuffer());
		mat.setTexture("NormalBuffer", gameState.getNormalBuffer());
		plane.setMaterial(mat);
		mainNode.attachChild(plane);
		
		propeller = assetManager.loadModel("models/propeller.blend");
		Material mat2 = new Material(assetManager, "shaders/Cartoon.j3md");
		mat2.setColor("Color", ColorRGBA.Green);
		mat2.setColor("AmbientLight", ColorRGBA.Green.mult(0.2f));
		mat2.setVector3("AmbientLightDirection", gameState.getLightDirection());
		mat2.setTexture("DepthBuffer", gameState.getDepthBuffer());
		mat2.setTexture("NormalBuffer", gameState.getNormalBuffer());
		propeller.setMaterial(mat2);
		propeller.rotate(pi/2, 0, 0);
		propeller.setLocalTranslation(0f, 0f, 4f);
		propeller.scale(0.5f);
		
		mainNode.attachChild(propeller);

		leftCollisionNode = new Node();
		rightCollisionNode = new Node();
		centerCollisionNode = new Node();
		leftCollisionNode.setLocalTranslation(-4.2f, -0.2f, 0);
		rightCollisionNode.setLocalTranslation(4.2f, -0.2f, 0);
		
		collisionMainNode = new Node();
		collisionMainNode.attachChild(leftCollisionNode);
		collisionMainNode.attachChild(rightCollisionNode);
		collisionMainNode.attachChild(centerCollisionNode);
		
		collisionNodes = new LinkedList<Node>();
		collisionNodes.add(leftCollisionNode);
		collisionNodes.add(rightCollisionNode);
		collisionNodes.add(centerCollisionNode);		
		
//		Sphere sphere = new Sphere(32, 32, 0.2f, true, false);
//		Geometry geo = new Geometry("ball", sphere);
//		mat = mat.clone();
//		mat.setColor("Color", ColorRGBA.Red);
//		geo.setMaterial(mat);
//
//		leftCollisionNode.attachChild(geo);
//		geo = new Geometry("ball", sphere);
//		geo.setMaterial(mat);
//		rightCollisionNode.attachChild(geo);
		
		mainNode.attachChild(collisionMainNode);
		
		Box bondBox = new Box(7,4,1f);
		boundBox = new Geometry("cube", bondBox);		
		mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		mat.setColor("Color", ColorRGBA.Green);
		boundBox.setMaterial(mat);
		mat.getAdditionalRenderState().setWireframe(true);
		boundBox.setCullHint(CullHint.Always);
		mainNode.attachChild(boundBox);
	}
	
	public LinkedList<Node> getCollisionNodes() {
		return collisionNodes;
	}
	
	public void reduceHealth(float damage) {
		health -= damage;
		gameState.getAudioNode(GameState.AUDIO_HURT).play();
	}
	
	public float getHealth()  {
		return health;
	}
	
	public void incSpeed() {
		maxSpeed += 0.1 * Math.pow(0.3f, maxSpeed);
		maxSideSpeed = 0.3f + maxSpeed/4f;
		maxVerticalSpeed = 0.15f + maxSpeed/4f;
		float pitch = Math.min(2.0f, 0.5f + maxSpeed * 0.25f);
		gameState.getAudioNode(GameState.AUDIO_MUSIC).setPitch(pitch);	
	}
	
	public void setAcceleratingUp(boolean up) {
		acceleratingUp = up;
	}
	
	public void setAcceleratingDown(boolean down) {
		acceleratingDown = down;
	}
	
	public void setAcceleratingLeft(boolean left) {
		acceleratingLeft = left;
	}
	
	public void setAcceleratingright(boolean right) {
		acceleratingRight = right;
	}
	
	public boolean contains(Node point) {
		return boundBox.getWorldBound().contains(point.getWorldTranslation());
	}

	public void update(float tpf) {
		
		
		if(gameState.isGameOver()) {
			mainNode.rotate(2 * tpf, 10 * tpf, 6*tpf);
			forceV.addLocal(0, -0.5f * tpf, 0.01f*tpf);
			mainNode.move(forceV.mult(tpf * 120f));
			return;
		}
		
		mainNode.setLocalRotation(Quaternion.DIRECTION_Z);
		collisionMainNode.setLocalRotation(Quaternion.DIRECTION_Z);
		boundBox.setLocalRotation(Quaternion.DIRECTION_Z);
		if (forceV.z < maxSpeed) {
			forceV.addLocal(0, 0, 0.1f * tpf);
		}

		if (acceleratingUp) {
			if (forceV.y < maxVerticalSpeed) {
				forceV.addLocal(0, 2.0f * tpf, 0);
			}
			mainNode.rotate(-(float) Math.PI / 8.0f, 0, 0);
			collisionMainNode.rotate(-(float) Math.PI / 8.0f, 0, 0);
			boundBox.rotate(-(float) Math.PI / 8.0f, 0, 0);
		} else {
			if (forceV.y > 0) {
				forceV.addLocal(0, -0.4f * tpf, 0);
			}
		}

		if (acceleratingDown) {
			if (forceV.y > -maxVerticalSpeed) {
				forceV.addLocal(0, -2.0f * tpf, 0);
			}
			mainNode.rotate((float) Math.PI / 8.0f, 0, 0);
			collisionMainNode.rotate((float) Math.PI / 8.0f, 0, 0);
			boundBox.rotate((float) Math.PI / 8.0f, 0, 0);
		} else {
			if (forceV.y < 0) {
				forceV.addLocal(0, 0.4f * tpf, 0);
			}
		}

		if (acceleratingRight) {
			if (forceV.x > -maxSideSpeed) {
				forceV.addLocal(-2.0f * tpf, 0, 0);
			}
			mainNode.rotate(0, 0, (float) Math.PI / 4.0f);
			collisionMainNode.rotate(0, 0, (float) Math.PI / 4.0f);
			boundBox.rotate(0, 0, (float) Math.PI / 4.0f);
		} else {
			if (forceV.x < 0) {
				forceV.addLocal(0.4f * tpf, 0, 0);
			}
		}

		if (acceleratingLeft) {
			if (forceV.x < maxSideSpeed) {
				forceV.addLocal(2.0f * tpf, 0, 0);
			}
			mainNode.rotate(0, 0, -(float) Math.PI / 4.0f);
			collisionMainNode.rotate(0, 0, -(float) Math.PI / 4.0f);
			boundBox.rotate(0, 0, -(float) Math.PI / 4.0f);
		} else {
			if (forceV.x > 0) {
				forceV.addLocal(-0.4f * tpf, 0, 0);
			}
		}
		mainNode.move(forceV.mult(tpf).mult(120f));
		propeller.rotate(0, 4.5f * tpf, 0);
		
		health = Math.min(1.0f, health += 0.01 * tpf);		
	}
}