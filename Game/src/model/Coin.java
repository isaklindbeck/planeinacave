package model;

import game.appStates.GameState;

import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.plugins.blender.BlenderModelLoader;

public class Coin extends Entity {

	public Coin(Vector3f pos, GameState gameState) {
		super(pos, gameState, 0, 0);
		rootNode = gameState.getToonNode();
	}

	@Override
	protected void initialize() {
		assetManager.registerLoader(BlenderModelLoader.class, "blend");
		Spatial coin = assetManager.loadModel("models/coin.blend");
		float pi = (float) Math.PI;
		coin.rotate(0, pi, 0);
		coin.scale(4.0f);
		
		Material mat = new Material(assetManager, "shaders/Cartoon.j3md");
		mat.setColor("Color", ColorRGBA.Orange);
		mat.setColor("AmbientLight", ColorRGBA.Yellow.mult(0.8f));
		mat.setVector3("AmbientLightDirection", gameState.getLightDirection());
		mat.setTexture("DepthBuffer", gameState.getDepthBuffer());
		mat.setTexture("NormalBuffer", gameState.getNormalBuffer());
		coin.setMaterial(mat);
		mainNode.attachChild(coin);
	}
	
	public boolean contains(Node point) {
		return false;
	}
	
	public void update(float tpf) {
		float pi = (float) Math.PI;
		mainNode.rotate(0, pi * tpf, 0);
	}
	
	public boolean isPlaced() {
		return mainNode.hasAncestor(rootNode);
	}

}
