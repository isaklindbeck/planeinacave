package control;

import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;

import model.Player;
import game.appStates.GameState;

public class Control {

	private GameState gameState;
	private Player player;
	private InputManager inputManager;

	public Control(GameState gameState, Player player) {
		this.gameState = gameState;
		this.player = player;
		inputManager = gameState.getInputManager();
		setupKeys();
	}
	
	public void setupKeys() {
		inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_UP));
		inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_DOWN));
		inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_LEFT));
		inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_RIGHT));
		inputManager.addMapping("Quit", new KeyTrigger(KeyInput.KEY_ESCAPE));
		
		inputManager.addListener(actionListener, "Up", "Down", "Left", "Right", "Quit");
	}
	
	public ActionListener getListener() {
		return actionListener;
	}
	
	protected ActionListener actionListener = new ActionListener() {

		@Override
		public void onAction(String name, boolean keyPressed, float tpf) {
			if (name.equals("Up")) {
				player.setAcceleratingUp(keyPressed);
			}
			if (name.equals("Down")) {
				player.setAcceleratingDown(keyPressed);
			}
			if (name.equals("Left")) {
				player.setAcceleratingLeft(keyPressed);
			}
			if (name.equals("Right")) {
				player.setAcceleratingright(keyPressed);
			}
			if(name.equals("Quit")) {
				gameState.quit();
			}
		}
		
	};
}
